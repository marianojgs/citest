﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Linq;
using System.ServiceProcess;
using System.Text;
using System.Diagnostics;
using System.Threading;

namespace RunScriptAsService
{
    public partial class RunScriptAsService : ServiceBase
    {
        string[] largs = null;
        public RunScriptAsService(string []args)
        {
            InitializeComponent();
            largs = args;
        }

        bool keep_running = true;
        Thread t = null;
        Process process = new Process();

        protected override void OnStart(string[] args)
        {           
            this.EventLog.WriteEntry("OnStart: " + this.ServiceName);
            if (args == null || args.Length==0)
                args = largs;

            if (args == null || args.Length == 0)
            {
                var err = "OnStart error, no arguments";
                this.EventLog.WriteEntry(err);
                throw new ArgumentNullException(err);
            }

            this.EventLog.WriteEntry("OnStart: " + string.Join(" ", args));

            if (t!=null)
            {
                keep_running = false;
                if (t.IsAlive)
                {
                    t.Join();
                }
                t = null;   
            }
            ProcessStartInfo info = new ProcessStartInfo();

            info.WindowStyle = ProcessWindowStyle.Hidden;
            info.FileName = args[0];
            info.UseShellExecute = true;
            if(args.Length>1)
                info.Arguments = string.Join(" ", args, 1, args.Length-1); 
            process.StartInfo = info;
            if(process.Start())
            {
                this.EventLog.WriteEntry("OnStart: " + this.ServiceName + " process started");

                keep_running = true;
                t = new Thread(new ThreadStart(() =>
                {
                    while(keep_running)
                    {                        
                        if (process.WaitForExit(200))
                        {
                            process = new Process();
                            process.StartInfo = info;
                            if(process.Start())
                            {
                                this.EventLog.WriteEntry("OnStart: " + this.ServiceName + " process started");
                            } else
                            {
                                this.EventLog.WriteEntry("OnStart: " + this.ServiceName + " start failed");
                            }
                        };
                        Thread.Sleep(1000);
                    }
                }));
                t.Start();
            }
        }

        protected override void OnStop()
        {
            this.EventLog.WriteEntry("OnStop: " + this.ServiceName);
            keep_running = false;
            if (process.WaitForExit(2000) == false)
                process.Kill();
            this.EventLog.WriteEntry("OnStop: " + this.ServiceName + " complete");
        }
    }
}
